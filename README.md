# Using the PresenterBuilder tool

## Requirements

* bash
* sed
* gsed
* rst2pdf

## Structure requirements

The buildtool requires 

* that there is a folder called `styles` with the styles `jsol_notes.style` and `jsol_slides.style` present
    * The style definition (provided in this repo) requires that there is a folder called `images` with the image `jsolslide_back2.pdf` in it
    * There is a properly formatted Reststructured text file called `fulldeck.rst` 
        * For note generation there needs to be a section for each slide in `fulldeck.rst` wrapped in the tags `[NOTES]` and `[END]`.
        * The buildtool will isolate the content between these two tags and create a `notes.pdf` file based on that content
    * For the rest of the content in `fulldeck.rst` the buildtool will generate a presentation-deck called `preso.pdf` 

The easiest way to fullfill these reqs. is probably to checkout this repo in full. It is already a full setup with a full sample.

## Running

To run the buildtool you invoke `./preparepreso.sh` and if everything is successful you will have a separate preso.pdf and a notes.pdf. Ready to plug into the PresenterConsole (https://bitbucket.org/benc_kau/presenterconsole).


