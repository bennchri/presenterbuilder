


===================================================
JSol Preso Demo using Restructured text and rst2pdf
===================================================

.. raw:: pdf

   PageBreak cutePage
   SetPageCounter 1 

.. footer:: ###Page### 


Slide with normal Bullets
=========================
* You are going to learn
    + how to develop in mule, so that you can have confidence to tackle any integration problem
    + how to debug applications so that you can ``quickly`` solve problems that come up
    + about available technologies that you can integrate with

[NOTES]
* This is the first note that is only relevant to the presenter
* This should only be visible to the instructor during presentation
* Should be removed in the actual preso preso
[END]


Slide with bullets and image above
==================================

.. image:: /Users/benc/projects/rest/rl/images/history.pdf

* You are going to learn
    + how to develop in mule, so that you can have confidence to tackle any integration problem
    + how to debug applications so that you can ``quickly`` solve problems that come up
    + about available technologies that you can integrate with

[NOTES]
.. class:: heading4

A classic arithmetical problem probably first posed by Euclid and investigated by various authors in the Middle Ages.
The problem is formulated as a dialogue between the two animals after which it is called. The mule say to the ass, "If you gave me one of your sacks, I would have as many as you." The ass replies, "If you gave one of your sacks, I would have twice as many as you." The question, of course, is "How many sacks do they have?" The number x of sacks of the mule and the number y of sacks of the ass are related by the identities:
[END]


Slide with bullets and image below
==================================


* You are going to learn
    + how to develop in mule, so that you can have confidence to tackle any integration problem
    + how to debug applications so that you can ``quickly`` solve problems that come up
    + about available technologies that you can integrate with

.. image:: /Users/benc/projects/rest/rl/images/history.pdf

[NOTES]
* Remember to tyr this out at home
* Please refer them to http://stackoverflow.com/questions/1258284/
is-it-possible-to-write-in-a-two-columns-style-in-restructuredtext
[END]

.. raw:: pdf
    
        PageBreak cuteTwoPage

Slide 2 cols
============

* You are going to learn
    + how to develop in mule, so that you can have confidence to tackle any integration problem
    + how to debug applications so that you can ``quickly`` solve problems that come up
    + about available technologies that you can integrate with

|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
    
.. image:: /Users/benc/projects/rest/rl/images/history.pdf
 
[NOTES]
* I haven't found a way to do so with ReST. 
    - You really should consider LaTeX for your research paper, especially for citations (BibTex), if you want to write it in plain text. 
    - You can easily switch between one and two columns: 
[END]

.. raw:: pdf

    PageBreak cutePage

Agenda
======
        
*  Day 1 
    +  Introduction
        +  Mule and ESB
        +  Basic Mule Elements
        +  Introduction to the Training Use Case 
        +  Mule Elements
    +  Unit Testing and Exchange Patters 
    +  Routing
    +  Properties, Variables, and Expressions 
    +  HTTP and Transports

[NOTES]
Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.

The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.
[END]


Slide with XML Stanza
=====================

* You are going to learn
    + how to develop in mule, so that you can have confidence to tackle any integration problem
    + how to debug applications so that you can ``quickly`` solve problems that come up
    + about available technologies that you can integrate with

   .. code-block:: xml

     <copy todir="${lib}" overwrite="false">
            <fileset dir="${carbon.home}/lib">
                <include name="commons-*.jar"/>
                <include name="backport-util-concurrent-*.jar"/>
            </fileset>
        </copy>

        <move todir="${lib}">
            <fileset dir="${lib}"/>
            <mapper>
                <mapper type="regexp" from="(.*).wso2v(.*)" to="\1-wso2v\2" />
                <mapper type="regexp" from="(.*).SNAPSHOT(.*)" to="\1\2" />
            </mapper>
        </move>
 
[NOTES]
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
[END]



Guidos Amazon presentation
==========================

- Borrow ideas whenever it makes sense
- As simple as possible, no simpler (Einstein)
- Do one thing well (UNIX)
- Don’t fret about performance (fix it later)
- Go with the flow (don’t fight environment)
  - Perfection is the enemy of the good
  - Cutting corners is okay (get back to it later)

|
|


  +--------------------------------+
  | .. class:: heading1            |
  |                                |
  | A noteworthy text in a box     |
  +--------------------------------+

[NOTES]
[END]

User-centric Design Philosopy
=============================

- Do one thing well (UNIX)
- Don’t fret about performance (fix it later)
- Go with the flow (don’t fight environment)
- Perfection is the enemy of the good
- Cutting corners is okay (get back to it later)

.. epigraph::
   
    **21 Years of Python.**

    From Pet Project to Programming Language of the year
    
    -- Guido Van Rossum May 2011

[NOTES]
There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.
[END]

Tables
======

=====  =====  ====== 
   Inputs     Output 
------------  ------ 
  A      B    A or B 
=====  =====  ====== 
False  False  False 
True   False  True 
False  True   True 
True   True   True 
=====  =====  ======

[NOTES]

[END]


