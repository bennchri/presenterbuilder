#!/bin/bash

sed -n -e "/\[NOTES\]/,/\[END\]/ p" fulldeck.rst > notes.rst
gsed 's/\[NOTES\]/.. raw:: pdf \n\n   PageBreak\n/g' notes.rst > pre_notes.rst
gsed 's/\[END\]/\n/g' pre_notes.rst > notes.rst
sed -n -e "/\[NOTES\]/,/\[END\]/ !p" fulldeck.rst > preso.rst
rst2pdf -b1 --default-dpi=72 -s tango.style,styles/jsol_notes.style  notes.rst -o notes.pdf 
rst2pdf -b1 --default-dpi=72 -s tango.style,styles/jsol_slides.style  preso.rst -o preso.pdf
rm pre_notes.rst
rm notes.rst
rm preso.rst



